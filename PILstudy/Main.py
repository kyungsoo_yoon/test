from PIL import Image
import scipy as sp
from scipy import misc
import matplotlib.pylab as plt
import seaborn as sns

img_gray = sp.misc.face(gray=True)
print(img_gray.shape)

sns.heatmap(img_gray[50:100,50:100], annot=True, fmt="d", cmap=plt.cm.bone)
plt.axis("off")
plt.show()

# im2 = Image.new("RGB", (500,500), (200,200,200))
# im3 = Image.new("RGB", (200,200))
#
# im2.paste(im3, (20,20,220,220))
# im2.save("paste_result.jpg")

# 1 (1-bit pixels, black and white, stored with one pixel per byte)
# L (8-bit pixels, black and white)
# P (8-bit pixels, mapped to any other mode using a color palette)
# RGB (3x8-bit pixels, true color)
# RGBA (4x8-bit pixels, true color with transparency mask)
# CMYK (4x8-bit pixels, color separation)
# YCbCr (3x8-bit pixels, color video format)
# LAB (3x8-bit pixels, the L*a*b color space)
# HSV (3x8-bit pixels, Hue, Saturation, Value color space)
# I (32-bit signed integer pixels)
# F (32-bit floating point pixels)


# image = Image.new("RGB",(500,500),(255,255,255))
# im = image.load()
# (width,height) = image.size
#
# for i in range(0,width):
#     for j in range(0,height):
#         color=((i+j)*255)/(width+height)
#         im[i,j] = (0,0,int(color))

# image.save("pixel_result.jpg")
#
#
# image = Image.open("sample1.png")
#
# im = image.load()
#
# (width, height) = image.size
#
# for i in range(0,width):
#     if(im[i,i] != (255,255,255)):
#         print(im[i,i])
#         color = im[i,i]
#
# for i in range(0,width):
#     for j in range(0,height):
#         if(im[i,j] != (255,255,255) and im[i,j] != color):
#             im[i,j] = (255-color[0],255-color[1],255-color[2])
#
# image.save("result.jpg")
#
#
